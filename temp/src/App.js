/* eslint-disable jsx-a11y/anchor-is-valid */
import "./App.css";
import HeroSection from "./components/HeroSection";
// import Contact from "./components/Contact";
// import Footer from "./components/Footer";
// import Heading from "./components/Heading";
// import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import NavBar from "./components/NavBar";
// import Service from "./components/Services";
// import About from "./components/About";
// import Technologies from "./components/Technologies/Techologies";

function App() {
  return (
      <div className="App">
        <NavBar />

        <HeroSection />
        {/* <Contact />
        <Footer /> */}
      </div>
  );
}

export default App;
