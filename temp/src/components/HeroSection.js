import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
const HeroSection = () => {
  return (
    <section id="hero" className="d-flex align-items-center">
      <div className="container-fluid" >
        <div className="row justify-content-center">
          <div className="col-xl-5 col-lg-6 pt-3 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
            <h1>YOU IDEATE, WE CREATE!</h1>
            <h2>Looking to build a product for your startup?
              <span style={{fontWeight: "bold"}}>Your wait ends here!</span>
            </h2>
            <div><a href="#about" className="btn-get-started scrollto">Get Started</a></div>
          </div>
          <div className="col-xl-4 col-lg-6 order-1 order-lg-2 hero-img">
            <img src="../img/hero-img.png" className="img-fluid animated" alt="" />
          </div>
        </div>
      </div>
    </section>
  );
}

export default HeroSection;
