const express = require("express");
const nodemailer = require("nodemailer");
const bodyParser = require("body-parser");
const app = express();
const path = require("path");
const cors = require("cors");
// Middleware to parse request body
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.options("*", cors());
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});
app.get("/", (req, res) => {
  res.send("Welcome to Connle API!");
});

// app.get("/api", (req, res) => {
//   res.send({ express: "Hello From Express" });
// });
// Route to handle POST request
app.post("/api/dummy/contact", (req, res) => {
  const { name, email, subject, message } = req.body;
  console.log(req.body);
  res.status(200).json("Dummy Endpoint");
});
app.post("/api/contact", (req, res) => {
  // Get form data from request body
  const { name, email, subject, message } = req.body;
  console.log(req.body);

  // Create transporter object
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "connlecustomerconnect@gmail.com",
      pass: "ddmpyoczbsafhepf",
    },
  });

  // Create email message
  var mailOptions = {
    from: email,
    to: "connleofficialmail@gmail.com",
    subject: "New Message from Contact Form",
    text: `Name: ${name}\nEmail: ${email}\nSubject: ${subject}\nMessage: ${message}`,
  };

  // Send email
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
      res.status(500).json("We faced an issue, please try again later.");
    } else {
      console.log("Email sent: " + info.response);
      // redirect to connle.com
      // res.status(200).redirect("https://connle.com#contact");
    }
  });

  mailOptions = {
    from: "connlecustomerconnect@gmail.com",
    to: email,
    subject: "We have received your message",
    html: `<h3>Hi ${name},</h3> <p>Thank you for contacting us. We will get back to you as soon as possible.</p> <p>Regards,</p> <p>Connle Team</p><br><br><pre>Do not reply to this email. This is an auto-generated email.</pre>    `,
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
      res.status(500).json("We faced an issue, please try again later.");
    } else {
      console.log("Email sent: " + info.response);
      // redirect to connle.com
      // res.status(200).redirect("https://connle.com#contact");
      res.status(200).json("Email sent");
    }
  });
});

// Start server
app.listen(5000, () => {
  console.log("Server started on port 5000");
});
